import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Print from "./views/pages/Print";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Pages
const Home = React.lazy(() => import("./views/pages/Home"));

function App() {
  return (
    <Router>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route path="/print/:id">
            <Print />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </React.Suspense>
    </Router>
  );
}

export default App;
