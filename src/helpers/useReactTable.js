import { useState } from 'react'
import { useDispatch } from 'react-redux'

const useReactTable = ({
  actionFetch,
  defaultSorted = { key: 'id', value: 'asc' },
  defaultFilter = []
}) => {
  const dispatch = useDispatch()
  const [stateTable, setStateTable] = useState({
    data: [],
    page: 0,
    pageSize: 20,
    loading: false
  })
  const [filter, setFilter] = useState(defaultFilter)
  const [filterMap, setFilterMap] = useState({})

  const fetchData = async params => {
    setStateTable({ ...stateTable, loading: true })
    const result = await dispatch(actionFetch(params))
    const response = result.getResponse()
    if (result.isSuccessResponse()) {
      setStateTable({
        ...stateTable,
        page: response.data.page - 1,
        pages: response.data.pages,
        pageSize: response.data.limit,
        data: response.data.docs,
        loading: false
      })
    } else {
      setStateTable({ ...stateTable, loading: false })
    }
  }

  const handleFetchData = async state => {
    setStateTable(state)
    const params = {}
    params['limit'] = state.pageSize
    params['page'] = state.page + 1
    params['filter'] = filter
    if (state.sorted !== null) {
      const sorted = state.sorted[0]
      let finalSort = {}
      if (sorted === undefined) {
        finalSort = defaultSorted
      } else {
        finalSort = {
          key: sorted && sorted.id,
          value: sorted && sorted.desc ? 'desc' : 'asc'
        }
      }
      params['sort'] = finalSort
    }

    await fetchData(params)
  }

  const handleChangeFilter = async (e, doFetch = true) => {
    const newFilterMap = {
      ...filterMap,
      [e.target.name]: e.target.value
    }

    const keys = Object.keys(newFilterMap)
    const finalFilter = []
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i]
      if (newFilterMap[key] !== '') {
        finalFilter.push({ key, value: newFilterMap[key] })
      }
    }
    setFilter(finalFilter)
    setFilterMap(newFilterMap)
    if (doFetch) {
      const params = {}
      params['page'] = 1
      params['limit'] = stateTable.pageSize
      params['filter'] = finalFilter
      await fetchData(params)
    }
  }

  return {
    stateTable,
    filterMap,
    handleChangeFilter,
    handleFetchData
  }
}

export default useReactTable
