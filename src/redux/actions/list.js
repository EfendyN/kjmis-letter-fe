import { API, setHeaderAuth } from "../../config/api";
import {
  LIST_INDEX,
  GET_DETAIL,
  GET_RINCIAN,
  ADD_PESANAN,
  GET_PROVINSI,
  GET_KOTA,
} from "../../config/constants";
import { connect } from "react-redux";

export const listIndex = (values) => {
  const {
    asal_pesanan,
    tgl_surat_pesanan,
    no_surat_pesanan,
    tujuan_provinsi,
    no_surat_tujuan,
    tgl_surat_tujuan,
    daerah_tujuan,
    tgl_pengambilan,
    status,
  } = values;
  return {
    type: LIST_INDEX,
    payload: async () => {
      const res = await API.post(
        `/orders?asal_pesanan=${asal_pesanan}&tgl_surat_pesanan=${tgl_surat_pesanan}&no_surat_pesanan=${no_surat_pesanan}&tujuan_provinsi=${tujuan_provinsi}&no_surat_tujuan=${no_surat_tujuan}&tgl_surat_tujuan=${tgl_surat_tujuan}&daerah_tujuan=${daerah_tujuan}&tgl_pengambilan=${tgl_pengambilan}&status=${status}`
      );
      const { data } = res.data;
      console.log(data, "asdasdas dasdasd asdas");
      return data;
    },
  };
};

export const getDetail = (id) => {
  return {
    type: GET_DETAIL,
    payload: async () => {
      const res = await API.get(`/order?order_id=${id}`);
      const { data } = res.data;
      console.log(data, "asdasdas dasdasd asdas");
      return data;
    },
  };
};

export const getRincianOrder = (id) => {
  return {
    type: GET_RINCIAN,
    payload: async () => {
      const res = await API.get(`/rincian?id=${id}`);
      const { data } = res.data;
      console.log(data, "asdasdas dasdasd asdas");
      return data;
    },
  };
};

export const addPesanan = (order, rincian) => {
  const list_rincian = [
    rincian.r1,
    rincian.r2,
    rincian.r3,
    rincian.r4,
    rincian.r5,
  ];
  console.log({ list_rincian }, "act orderxxx");
  return {
    type: ADD_PESANAN,
    payload: async () => {
      const res = await API.post(`/order`, { order, list_rincian });
      const { data } = res.data;
      console.log(data, "asdasdas dasdasd asdas");
      return data;
    },
  };
};

export const provinsiIndex = () => {
  return {
    type: GET_PROVINSI,
    payload: async () => {
      const res = await API.get(`/provinsi`);
      const { data } = res.data;
      return data;
    },
  };
};

export const kotaIndex = (provinsi) => {
  return {
    type: GET_KOTA,
    payload: async () => {
      const res = await API.post(`/kota?provinsi=${provinsi}`);
      const { data } = res.data;
      return data;
    },
  };
};

export const kotaBulogIndex = () => {
  return {
    type: "GET_BULOG",
    payload: async () => {
      const res = await API.get(`/bulog`);
      const { data } = res.data;
      return data;
    },
  };
};
