import {
  LIST_INDEX,
  ADD_PESANAN,
  GET_DETAIL,
  GET_RINCIAN,
} from "../../config/constants";

const initialState = {
  list: [],
  add: [],
  loading: true,
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case `${LIST_INDEX}_PENDING`:
    case `${GET_DETAIL}_PENDING`:
    case `${GET_RINCIAN}_PENDING`:
    case `${ADD_PESANAN}_PENDING`:
      return {
        ...state,
        loading: true,
      };
    case `${LIST_INDEX}_FULFILLED`:
    case `${GET_DETAIL}_FULFILLED`:
    case `${GET_RINCIAN}_FULFILLED`:
      return {
        ...state,
        list: action.payload,
        loading: false,
      };
    case `${LIST_INDEX}_REJECTED`:
    case `${GET_DETAIL}_REJECTED`:
    case `${GET_RINCIAN}_REJECTED`:
    case `${ADD_PESANAN}_REJECTED`:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case `${ADD_PESANAN}_FULFILLED`:
      return {
        ...state,
        add: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
