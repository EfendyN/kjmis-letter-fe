import { createStore, combineReducers, applyMiddleware } from "redux";

import list from "./reducers/list";

import logger from "redux-logger";
import promise from "redux-promise-middleware";

// Global state
const rootReducers = combineReducers({
  list,
});

// Setup store for Redux
const store = createStore(rootReducers, applyMiddleware(logger, promise));

export default store;
