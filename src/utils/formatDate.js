import Moment from 'moment'
import 'moment/locale/id'

Moment.locale('id');
export const formatDate = (date, format = 'DD/MM/YYYY HH:mm') => {
  return Moment(date).format(format).toString()
}