export const IS_DEV = process.env.NODE_ENV !== 'production'

export const API_VERSION = 'v1'

export const baseUrl = 'http://192.168.1.6:5000'

export * from './storage'
export * from './formatDate'
export * from './formatNumber'
