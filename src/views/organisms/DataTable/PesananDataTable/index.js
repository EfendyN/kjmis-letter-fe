import React, { useEffect, useState } from "react";
import ReactTable from "react-table-6";
import { Row, Col, Input, Button } from "reactstrap";
import { Link } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
// Helpers

// Utils
import { formatDate } from "../../../../utils";
// Actions
import { listIndex, getDetail } from "../../../../redux/actions/list";
import Print from "../../../pages/Print/index";
import { fromJS } from "immutable";

const PesananDataTable = ({ listIndex, getDetail, list, handleClikDetail }) => {
  const [stateTable, setStateTable] = useState({
    data: [],
    page: 0,
    pageSize: 20,
    loading: false,
  });
  const [filterStateTable, setFilterStateTable] = useState({
    data: [
      {
        asal_pesanan: "",
        tgl_surat_pesanan: "",
        no_surat_pesanan: "",
        tujuan_provinsi: "",
        no_surat_tujuan: "",
        tgl_surat_tujuan: "",
        daerah_tujuan: "",
        tgl_pengambilan: "",
        status: "",
      },
    ],
  });
  const [asal_pesanan, setAsal_pesanan] = useState("");
  const [tgl_surat_pesanan, setTgl_surat_pesanan] = useState("");
  const [no_surat_pesanan, setNo_surat_pesanan] = useState("");
  const [tujuan_provinsi, setTujuan_provinsi] = useState("");
  const [no_surat_tujuan, setNo_surat_tujuan] = useState("");
  const [tgl_surat_tujuan, setTgl_surat_tujuan] = useState("");
  const [daerah_tujuan, setDaerah_tujuan] = useState("");
  const [tgl_pengambilan, setTgl_pengambilan] = useState("");
  const [status, setStatus] = useState("");

  const values = {
    asal_pesanan,
    tgl_surat_pesanan,
    no_surat_pesanan,
    tujuan_provinsi,
    no_surat_tujuan,
    tgl_surat_tujuan,
    daerah_tujuan,
    tgl_pengambilan,
    status,
  };

  useEffect(() => {
    const data = async () => {
      const res = await listIndex(values);
      setStateTable({ data: res.action.payload });
    };
    data();
  }, []);

  // const handleChangeFilter = async (event) => {
  //   console.log(filterStateTable, "filteeerrrrr");
  //   const x = async () => {
  //     const res = await listIndex(filterStateTable);
  //     setStateTable({ data: res.action.payload });
  //   };
  // };

  return (
    <ReactTable
      data={stateTable.data}
      // page={stateTable.page}
      // pages={stateTable.pages}
      // loading={stateTable.loading}
      // defaultPageSize={stateTable.pageSize}
      filterable={true}
      manual={true}
      // onFetchData={handleFetchData}
      className="-striped -highlight"
      columns={[
        {
          Header: "Aksi",
          filterable: false,
          sortable: false,
          width: 150,
          Cell: (row) => {
            return (
              <Row>
                <Col className="text-center">
                  <Button
                    color="success"
                    size="sm"
                    onClick={async () => {
                      const x = await getDetail(row.original.id);
                      if (x) {
                        handleClikDetail(row.original);
                      }
                    }}
                  >
                    {/* <i className="fa fa-edit" /> */}Detail
                  </Button>{" "}
                  <Link to={`/print/${row.original.id}`}>
                    <Button color="success" size="sm">
                      {/* <i className="fa fa-edit" /> */}Print
                    </Button>
                  </Link>
                </Col>
              </Row>
            );
          },
        },
        {
          Header: "ID",
          accessor: "id",
          width: 50,
          Filter: () => <Input name="id" />,
        },
        {
          Header: "Asal Pesanan",
          accessor: "asal_pesanan",
          Filter: () => (
            <Input
              name="asal_pesanan"
              // onKeyPress={(event) => {
              //   if (event.keyCode === 13 || event.which === 13) {
              //     setAsal_pesanan(event.target.value);
              //     console.log(values, "aaaaaasssxxxxx");
              //     listIndex(values);
              //   }
              // }}
            />
          ),
        },
        {
          Header: "Tanggal Surat Pesanan",
          accessor: "tgl_surat_pesanan",
          Filter: () => <Input name="tgl_surat_pesanan" />,
          Cell: (row) =>
            formatDate(row.original.tgl_surat_pesanan, "DD/MM/YYYY"),
        },
        {
          Header: "Provinsi",
          accessor: "tujuan_provinsi",
          Filter: () => <Input name="tujuan_provinsi" />,
        },
        {
          Header: "No Surat Tujuan",
          accessor: "no_surat_tujuan",
          Filter: () => <Input name="no_surat_tujuan" />,
        },
        {
          Header: "Tgl Surat Tujuan",
          accessor: "tgl_surat_tujuan",
          Filter: () => <Input name="tgl_surat_tujuan" />,
          Cell: (row) =>
            formatDate(row.original.tgl_surat_tujuan, "DD/MM/YYYY"),
        },
        {
          Header: "Daerah Tujuan",
          accessor: "daerah_tujuan",
          Filter: () => <Input name="daerah_tujuan" />,
        },

        {
          Header: "Tgl Pengambilan",
          accessor: "tgl_pengambilan",
          Filter: () => <Input name="tgl_pengambilan" />,
          Cell: (row) => formatDate(row.original.tgl_pengambilan, "DD/MM/YYYY"),
        },
        {
          Header: "Status",
          accessor: "status",
          Filter: () => <Input name="status" />,
          Cell: (row) => {
            if (row.original.status === "Tunggakan") {
              return (
                <div
                  style={{
                    backgroundColor: "red",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikDetail(row.original)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.status}
                  </label>
                </div>
              );
            } else {
              return (
                <div
                  style={{
                    backgroundColor: "green",
                    textAlign: "center",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={() => handleClikDetail(row.original)}
                >
                  <label
                    style={{
                      cursor: "pointer",
                    }}
                  >
                    {row.original.status}
                  </label>
                </div>
              );
            }
          },
        },
      ]}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listIndex: (values) => dispatch(listIndex(values)),
    getDetail: (id) => dispatch(getDetail(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PesananDataTable);
