import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Table,
  Button,
  Card,
  CardBody,
  CardHeader,
  ModalFooter,
} from "reactstrap";
import PropTypes from "prop-types";
import { formatDate, formatNumber } from "../../../utils";
import { getRincianOrder } from "../../../redux/actions/list";
import { connect } from "react-redux";

const DetailModal = (props) => {
  const { isOpen, toggle, data } = props;
  const [next, setNext] = useState(false);
  const [bname, setBname] = useState("Rincian Pesanan");
  const [stateRincian, setStateRincian] = useState({
    rincian: [],
  });

  useEffect(() => {
    const data = async () => {
      const res = await props.getRincianOrder(props.id);
      setStateRincian({ rincian: res.action.payload });
    };
    data();
  }, []);

  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size="lg"
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Detail Pemesanan</ModalHeader>
      <ModalBody>
        {next ? (
          <Table bordered>
            {props.list.list.rincian && (
              <>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Unit</th>
                    <th>Jumlah Barang</th>
                    <th>Harga perunit</th>
                    <th>Total Harga</th>
                    <th>HET</th>
                  </tr>
                </thead>
                <tbody>
                  {props.list.list.rincian.map((item, index) => (
                    <tr key={index}>
                      <th>{index + 1}</th>
                      <td scope="row">{item.nama_barang}</td>
                      <td scope="row">
                        {item.unit} {item.unit_type}
                      </td>
                      <td scope="row">{item.jumlah}</td>
                      <td scope="row">Rp{formatNumber(item.harga_perunit)}</td>
                      <td scope="row">Rp{formatNumber(item.total_harga)}</td>
                      <td scope="row">Rp{formatNumber(item.het)}</td>
                    </tr>
                  ))}
                </tbody>
              </>
            )}
          </Table>
        ) : (
          <>
            <Card>
              <CardHeader>
                <strong>Surat Asal</strong>
              </CardHeader>
              <CardBody>
                <Table bordered>
                  {data && (
                    <tbody>
                      <tr>
                        <td>Asal Pesanan</td>
                        <td>{data.asal_pesanan}</td>
                      </tr>
                      <tr>
                        <td>No Surat Pesanan</td>
                        <td>{data.no_surat_pesanan}</td>
                      </tr>
                      <tr>
                        <td>Tanggal Surat Pesanan</td>
                        <td>
                          {formatDate(data.tgl_surat_pesanan, "DD MMMM YYYY")}
                        </td>
                      </tr>
                    </tbody>
                  )}
                </Table>
              </CardBody>
            </Card>
            <Card>
              <CardHeader>
                <strong>Surat Tujuan</strong>
              </CardHeader>
              <CardBody>
                <Table bordered>
                  {data && (
                    <tbody>
                      <tr>
                        <td>Provinsi</td>
                        <td>{data.tujuan_provinsi}</td>
                      </tr>
                      <tr>
                        <td>No Surat</td>
                        <td>{data.no_surat_tujuan}</td>
                      </tr>
                      <tr>
                        <td>Tanggal Surat</td>
                        <td>
                          {formatDate(data.tgl_surat_tujuan, "DD MMMM YYYY")}
                        </td>
                      </tr>
                      <tr>
                        <td>Daerah Tujuan</td>
                        <td>{data.daerah_tujuan}</td>
                      </tr>
                    </tbody>
                  )}
                </Table>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <Table bordered>
                  {data && (
                    <tbody>
                      <tr>
                        <td>Tanggal Pengambilan</td>
                        <td>
                          {formatDate(data.tgl_pengambilan, "DD MMM YYYY")}
                        </td>
                      </tr>
                      <tr>
                        <td>Status</td>
                        {data.status === "Lunas" ? (
                          <td
                            style={{
                              backgroundColor: "green",
                              color: "white",
                              textAlign: "center",
                            }}
                          >
                            {data.status}
                          </td>
                        ) : (
                          <td
                            style={{
                              backgroundColor: "red",
                              color: "white",
                              textAlign: "center",
                            }}
                          >
                            {data.status}
                          </td>
                        )}
                      </tr>
                    </tbody>
                  )}
                </Table>
              </CardBody>
            </Card>
          </>
        )}
        <ModalFooter>
          <Button
            color="success"
            onClick={() => {
              setNext(!next);
              setBname(
                bname === "Rincian Pesanan"
                  ? "Rincian Surat"
                  : "Rincian Pesanan"
              );
            }}
          >
            {bname}
          </Button>
        </ModalFooter>
      </ModalBody>
    </Modal>
  );
};

DetailModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  data: PropTypes.object,
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRincianOrder: (id) => dispatch(getRincianOrder(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailModal);
