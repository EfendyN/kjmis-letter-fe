import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  Table,
  Card,
  CardBody,
  CardHeader,
  Alert,
} from "reactstrap";
import { Form } from "react-bootstrap";
// import rincianBarang from "../../../constans/imputRincian";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import {
  addPesanan,
  provinsiIndex,
  kotaIndex,
  kotaBulogIndex,
} from "../../../redux/actions/list";

const TambahPesananModal = ({
  isOpen,
  toggle,
  addPesanan,
  provinsiIndex,
  kotaIndex,
  kotaBulogIndex,
}) => {
  const [next, setNext] = useState(false);
  const [btext, setbtext] = useState("Rincian Pesanan");
  const [tittle, setTittle] = useState("Pemesanan");
  const [jumlahBarang, setJumlahBarang] = useState(1);
  const [alert, setAlert] = useState(false);

  const [provKota, setProvKota] = useState({ data: [] });
  const [kota, setKota] = useState({ data: [] });
  const [kotaBulog, setKotaBulog] = useState({ data: [] });
  const [bulog, setBulog] = useState("");
  const [provinsi, setProvinsi] = useState("");

  const [order, setOrder] = useState({
    asal_pesanan: null,
    tgl_surat_pesanan: null,
    no_surat_pesanan: null,
    tujuan_provinsi: null,
    no_surat_tujuan: null,
    tgl_surat_tujuan: null,
    daerah_tujuan: null,
    tgl_pengambilan: null,
  });
  const [rincian, setRincian] = useState({
    r1: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r2: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r3: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r4: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r5: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r6: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r7: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r8: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r9: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
    r10: {
      nama_barang: null,
      unit: null,
      unit_type: null,
      jumlah: null,
      harga_perunit: null,
      total_harga: null,
      het: null,
    },
  });

  useEffect(() => {
    const data = async () => {
      const res = await provinsiIndex();
      setProvKota({ data: res.action.payload });
    };
    const dataKota = async () => {
      const res = await kotaIndex(provinsi);
      setKota({ data: res.action.payload });
    };
    const dataKotaBulog = async () => {
      const res = await kotaBulogIndex();
      setKotaBulog({ data: res.action.payload });
      console.log(res, "kota bulog");
    };
    dataKota();
    dataKotaBulog();
    data();
  }, []);

  const handleChangeFormOrder = (e) => {
    setOrder({ ...order, [e.target.name]: e.target.value });
  };

  const handleSubmit = async () => {
    const x = await addPesanan(order, rincian);
    if (x) {
      // window.location.reload();
      console.log(order, rincian);
    }
  };

  const hcRincian1 = (e) => {
    setRincian({
      ...rincian,
      r1: {
        ...rincian.r1,
        [e.target.name]: e.target.value,
      },
    });
  };
  const hcRincian2 = (e) => {
    setRincian({
      ...rincian,
      r2: {
        ...rincian.r2,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian3 = (e) => {
    setRincian({
      ...rincian,
      r3: {
        ...rincian.r3,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian4 = (e) => {
    setRincian({
      ...rincian,
      r4: {
        ...rincian.r4,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian5 = (e) => {
    setRincian({
      ...rincian,
      r5: {
        ...rincian.r5,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian6 = (e) => {
    setRincian({
      ...rincian,
      r6: {
        ...rincian.r6,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian7 = (e) => {
    setRincian({
      ...rincian,
      r7: {
        ...rincian.r7,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian8 = (e) => {
    setRincian({
      ...rincian,
      r8: {
        ...rincian.r8,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian9 = (e) => {
    setRincian({
      ...rincian,
      r9: {
        ...rincian.r9,
        [e.target.name]: e.target.value,
      },
    });
  };

  const hcRincian10 = (e) => {
    setRincian({
      ...rincian,
      r10: {
        ...rincian.r10,
        [e.target.name]: e.target.value,
      },
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      backdrop="static"
      size={next ? "lg" : "md"}
      keyboard={true}
    >
      <ModalHeader toggle={toggle}>Tambah {tittle}</ModalHeader>
      <ModalBody>
        {next ? (
          <>
            <Form>
              <Table bordered>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Unit</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Per Unit</th>
                    <th>HET</th>
                  </tr>
                </thead>

                {/* Rincian 1 */}
                <tbody>
                  {jumlahBarang >= 1 ? (
                    <tr>
                      <th scope="row">1</th>
                      <td>
                        <FormGroup>
                          <Input
                            name="nama_barang"
                            onChange={(e) => hcRincian1(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="unit" onChange={(e) => hcRincian1(e)} />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="jumlah_barang"
                            onChange={(e) => hcRincian1(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="harga_perunit"
                            onChange={(e) => hcRincian1(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="het" onChange={(e) => hcRincian1(e)} />
                        </FormGroup>
                      </td>
                    </tr>
                  ) : null}

                  {/* Rincian 2 */}
                  {jumlahBarang >= 2 ? (
                    <tr>
                      <th scope="row">2</th>
                      <td>
                        <FormGroup>
                          <Input
                            name="nama_barang"
                            onChange={(e) => hcRincian2(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="unit" onChange={(e) => hcRincian2(e)} />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="jumlah_barang"
                            onChange={(e) => hcRincian2(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="harga_perunit"
                            onChange={(e) => hcRincian2(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="het" onChange={(e) => hcRincian2(e)} />
                        </FormGroup>
                      </td>
                    </tr>
                  ) : null}

                  {/* Rincian 3 */}
                  {jumlahBarang >= 3 ? (
                    <tr>
                      <th scope="row">3</th>
                      <td>
                        <FormGroup>
                          <Input
                            name="nama_barang"
                            onChange={(e) => hcRincian3(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="unit" onChange={(e) => hcRincian3(e)} />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="jumlah_barang"
                            onChange={(e) => hcRincian3(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="harga_perunit"
                            onChange={(e) => hcRincian3(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="het" onChange={(e) => hcRincian3(e)} />
                        </FormGroup>
                      </td>
                    </tr>
                  ) : null}

                  {/* Rincian 4 */}
                  {jumlahBarang >= 4 ? (
                    <tr>
                      <th scope="row">4</th>
                      <td>
                        <FormGroup>
                          <Input
                            name="nama_barang"
                            onChange={(e) => hcRincian4(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="unit" onChange={(e) => hcRincian4(e)} />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="jumlah_barang"
                            onChange={(e) => hcRincian4(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="harga_perunit"
                            onChange={(e) => hcRincian4(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="het" onChange={(e) => hcRincian4(e)} />
                        </FormGroup>
                      </td>
                    </tr>
                  ) : null}

                  {/* Rincian 5 */}
                  {jumlahBarang >= 5 ? (
                    <tr>
                      <th scope="row">5</th>
                      <td>
                        <FormGroup>
                          <Input
                            name="nama_barang"
                            onChange={(e) => hcRincian5(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="unit" onChange={(e) => hcRincian5(e)} />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="jumlah_barang"
                            onChange={(e) => hcRincian5(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input
                            name="harga_perunit"
                            onChange={(e) => hcRincian5(e)}
                          />
                        </FormGroup>
                      </td>
                      <td>
                        <FormGroup>
                          <Input name="het" onChange={(e) => hcRincian5(e)} />
                        </FormGroup>
                      </td>
                    </tr>
                  ) : null}
                </tbody>
              </Table>
            </Form>
            <Button
              onClick={() =>
                setJumlahBarang(
                  jumlahBarang != 1 ? jumlahBarang - 1 : jumlahBarang
                )
              }
            >
              -
            </Button>
            <Button
              onClick={() => {
                setJumlahBarang(
                  jumlahBarang < 5 ? jumlahBarang + 1 : jumlahBarang
                );
                console.log(rincian, "Asdasd rincian");
              }}
            >
              +
            </Button>
          </>
        ) : (
          <Form>
            {alert ? (
              <Alert color="danger">Harap isi Form yang kosong</Alert>
            ) : null}
            <Card>
              <CardHeader>
                <strong sm={4}>Asal Pesanan</strong>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Label sm={4}>Asal Pesanan</Label>
                  <Col sm={8}>
                    <Form.Control
                      as="select"
                      name="asal_pesanan"
                      onChange={(e) => handleChangeFormOrder(e)}
                    >
                      <option>Pilih Kota/Kabupaten</option>
                      {kota.data.map((item, index) => (
                        <option key={index}>{item.nama}</option>
                      ))}
                    </Form.Control>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>Tanggal Pemesanan</Label>
                  <Col sm={8}>
                    <Input
                      name="tgl_surat_pesanan"
                      type="date"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>Nomor Pesanan</Label>
                  <Col sm={8}>
                    <Input
                      name="no_surat_pesanan"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                <strong>Tujuan Pesanan</strong>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Label sm={4}>Pesanan Kepada</Label>
                  <Col sm={8}>
                    <Form.Control
                      as="select"
                      name="tujuan_provinsi"
                      onChange={(e) => handleChangeFormOrder(e)}
                    >
                      <option>Pilih Provinsi</option>
                      {kotaBulog.data.map((item, index) => (
                        <option key={index}>{item.provinsi.nama}</option>
                      ))}
                    </Form.Control>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>No Surat</Label>
                  <Col sm={8}>
                    <Input
                      name="no_surat_tujuan"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>Tanggal Surat</Label>
                  <Col sm={8}>
                    <Input
                      type="date"
                      name="tgl_surat_tujuan"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>Untuk Daerah {bulog}</Label>
                  <Col sm={8}>
                    <Form.Control
                      as="select"
                      name="daerah_tujuan"
                      onChange={(e) => handleChangeFormOrder(e)}
                    >
                      <option>Pilih Kota/Kabupaten</option>
                      {kota.data.map((item, index) => (
                        <option key={index}>{item.nama}</option>
                      ))}
                    </Form.Control>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={4}>Tanggal Pengambilan</Label>
                  <Col sm={8}>
                    <Input
                      name="tgl_pengambilan"
                      type="date"
                      onChange={(e) => handleChangeFormOrder(e)}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Form>
        )}
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={() => {
            if (
              order.asal_pesanan === null ||
              order.daerah_tujuan === null ||
              order.no_surat_pesanan === null ||
              order.no_surat_tujuan === null ||
              order.tgl_pengambilan === null
            ) {
              setAlert(true);
            } else {
              setNext(!next);
              setbtext(
                btext === "Rincian Pesanan" ? "Kembali" : "Rincian Pesanan"
              );
              setTittle(tittle === "Pemesanan" ? "Barang" : "Pemesanan");
            }
          }}
        >
          {btext}
        </Button>
        {next ? <Button onClick={handleSubmit}>Tambah Pesanan</Button> : null}
      </ModalFooter>
    </Modal>
  );
};

TambahPesananModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  data: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => {
  return {
    provinsiIndex: () => dispatch(provinsiIndex()),
    kotaBulogIndex: () => dispatch(kotaBulogIndex()),
    kotaIndex: (provinsi) => dispatch(kotaIndex(provinsi)),
    addPesanan: (order, rincian) => dispatch(addPesanan(order, rincian)),
  };
};
export default connect(null, mapDispatchToProps)(TambahPesananModal);
