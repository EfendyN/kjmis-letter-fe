import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
// Organisms
import DataTable from "../../organisms/DataTable/PesananDataTable";
import ModalDetail from "../../organisms/ModalDetail";
import ModalTambahPemesanan from "../../organisms/ModalTambahPemesanan";

const Home = () => {
  const [modalDetailIsOpen, setModalDetailIsOpen] = useState(false);
  const [activeData, setActiveData] = useState(null);

  const [modalTambahPemesanan, setModalTambahPemesanan] = useState(null);

  const toggleModalTambahPemesanan = () => {
    setModalTambahPemesanan(!modalTambahPemesanan);
  };

  const handleClikTambah = () => {
    toggleModalTambahPemesanan();
  };

  const toggleModalDetail = () => {
    if (modalDetailIsOpen) {
      setActiveData(null);
    }
    setModalDetailIsOpen(!modalDetailIsOpen);
  };

  const handleClikDetail = (data) => {
    setActiveData(data);
    toggleModalDetail();
  };

  return (
    <div className="animated fadeIn">
      <Helmet>
        <title>KJMIS</title>
      </Helmet>
      <Row>
        <Col xs="12" lg="12">
          <Card>
            <CardHeader>
              Data Pesanan{" "}
              <Button
                outline
                color="primary"
                size="sm"
                onClick={handleClikTambah}
              >
                <i className="fa fa-plus" /> Tambah Pesanan
              </Button>
            </CardHeader>
            <CardBody>
              <DataTable handleClikDetail={handleClikDetail} />
            </CardBody>
          </Card>
        </Col>
      </Row>

      <ModalDetail
        mode="edit"
        isOpen={modalDetailIsOpen}
        toggle={toggleModalDetail}
        data={activeData}
      />

      <ModalTambahPemesanan
        mode="edit"
        isOpen={modalTambahPemesanan}
        toggle={toggleModalTambahPemesanan}
      />
    </div>
  );
};

export default Home;
