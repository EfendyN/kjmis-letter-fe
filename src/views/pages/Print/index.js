import React, { useRef, useEffect, useState } from "react";
import ReactToPrint from "react-to-print";
import { Button, Row, Col } from "react-bootstrap";
import "./print.css";
import { useParams } from "react-router-dom";
import { getDetail } from "../../../redux/actions/list";
import { connect } from "react-redux";
import { formatDate } from "../../../utils/formatDate";

class ComponentToPrint extends React.Component {
  render() {
    const { data, rincian } = this.props;
    return (
      <div
        style={{
          position: "relative",
          top: "200px",
          marginBottom: 150,
          fontSize: 33,
        }}
      >
        <Row>
          <Col className="col1">
            Jakarta , {formatDate(data && data.tgl_asal_surat, "DD MMMM YYYY")}
          </Col>
        </Row>
        <Row>
          <Col className="col2">Nomor : {data && data.no_surat_pesanan}</Col>
        </Row>
        <Row>
          <Col className="col3">Perihal : Pemesanan Komoditi Pangan</Col>
        </Row>
        <Row>
          <Col className="col4">Kepada</Col>
        </Row>
        <Row>
          <Col className="col5">
            Yth :{" "}
            <span style={{ fontWeight: 800 }}>
              Pimpinan Wilayah BULOG {data && data.tujuan_provinsi}
            </span>
          </Col>
        </Row>
        <Row>
          <Col className="col6">Di {data && data.daerah_tujuan}</Col>
        </Row>
        <Row>
          <Col className="col7">
            Sehubungan dengan kerjasama penyediaan komoditi pangan oleh Bulog ke
            Jaringan Warung
            <br />
            <span style={{ wordSpacing: 7 }}>
              {" "}
              KJMIS, bersama ini kami sampaikan bahwa kami membutuhkan komoditi
              Beras Medium.{" "}
            </span>
            <br /> Mohon kiranya Bulog dapat menyediakan komoditi pangan sbb:{" "}
          </Col>
        </Row>
        <Row>
          <Col className="col8">{data && data.daerah_tujuan} </Col>
        </Row>
        <Row>
          <Col className="col9">
            <table style={{ fontSize: 35 }}>
              <tr>
                <th>No</th>
                <th>Komoditi</th>
                <th>Unit</th>
                <th>Jumlah</th>
              </tr>
              {rincian &&
                rincian.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.nama_barang}</td>
                    <td>
                      {item.unit} {item.unit_type}
                    </td>
                    <td>{item.jumlah}</td>
                  </tr>
                ))}
            </table>
          </Col>
        </Row>
        <Row>
          <Col className="col10">
            Mohon paket tersebut disiapkan, dan akan kami jemput tanggal
            (d+1/NextDay)
            <br /> Demikian permintaan barang kami, atas perhatian dan
            kerjasamanya terima kasih
          </Col>
        </Row>
        <Row>
          <Col className="col11">Hormat kami, </Col>
        </Row>
        <Row>
          <Col className="col12">Imam Natsir </Col>
        </Row>
        <Row>
          <Col className="col13">Direktur KJMIS</Col>
        </Row>
        <Row>
          <Col className="col14">Tembusan :</Col>
        </Row>
        <Row>
          <Col className="col15">1. Ketua Pengurus KJMIS </Col>
        </Row>
        <Row>
          <Col className="col15">2. Direktur Komersil BULOG </Col>
        </Row>
      </div>
    );
  }
}

const Print = ({ getDetail, list }) => {
  const componentRef = useRef();
  const { id } = useParams();

  const [data, setData] = useState();

  useEffect(() => {
    const dis = async () => {
      const res = await getDetail(id);
      setData(res.action.payload.rincian);
      console.log(res.action.payload.rincian, data, "dataaaaa");
    };
    dis();
  }, []);

  return (
    <div>
      <ReactToPrint
        trigger={() => (
          <Button style={{ marginTop: 10, marginLeft: 10 }}>Print</Button>
        )}
        content={() => componentRef.current}
      />
      <ComponentToPrint
        data={list.list.order}
        rincian={list.list.rincian}
        ref={componentRef}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetail: (id) => dispatch(getDetail(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Print);
